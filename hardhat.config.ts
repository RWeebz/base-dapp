import { HardhatUserConfig } from 'hardhat/config';
import '@typechain/hardhat';
import '@nomiclabs/hardhat-waffle';
import '@nomiclabs/hardhat-ethers';
import 'hardhat-deploy-ethers';
import 'hardhat-deploy';
import '@typechain/ethers-v5';
import path from 'path';

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
const config: HardhatUserConfig = {
  defaultNetwork: 'hardhat',
  networks: {
    hardhat: {
      loggingEnabled: true,
      chainId: 1337,
      live: true,
    },
    testnet: {
      url: 'https://data-seed-prebsc-1-s1.binance.org:8545',
      chainId: 97,
      gasPrice: 10000000000,
      loggingEnabled: true,
      accounts: {
        mnemonic: 'bjir',
      },
    },
  },
  solidity: {
    compilers: [
      {
        version: '0.8.7',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  paths: {
    artifacts: `${path.join(__dirname + '/frontend/typechain/artifacts')}`,
    cache: `${path.join(__dirname + '/cache')}`,
    sources: `${path.join(__dirname + '/contracts')}`,
    tests: `${path.join(__dirname + '/test')}`,
  },
  typechain: {
    target: 'ethers-v5',
    outDir: `${path.join(__dirname + '/frontend/typechain/types')}`,
    alwaysGenerateOverloads: false,
  },
};

export default config;
