import React from 'react';
import styled from 'styled-components';
import * as s from './styles/globalStyles';
import Homepage from './components/Homepage';

export const StyledButton = styled.button`
  padding: 8px;
`;

export default function App() {
  return (
    <s.Screen>
      <Homepage />
    </s.Screen>
  );
}
