import { ethers } from 'ethers';
import { Dispatch } from 'redux';
import {
  connectionDataDispatchTypes,
  connectionDispatchTypes,
  connectionFailed,
  connectionRequest,
  connectionSuccess,
  updateAccount,
  defaultState,
  fetchData,
} from '../index';
import SmartContract from '../../typechain/artifacts/contracts/SmartContract.sol/SmartContract.json';
import HatSwapJson from '../../typechain/artifacts/contracts/HatSwap.sol/HatSwap.json';
import { HatSwapInterface, HatSwap } from '../../typechain/types/HatSwap';

const connectRequest = (): connectionRequest => {
  return {
    type: 'CONNECTION_REQUEST',
  };
};

const connectSuccess = (
  payload: string | undefined | defaultState,
): connectionSuccess => {
  return {
    type: 'CONNECTION_SUCCESS',
    payload: payload,
  };
};

const connectFailed = (
  payload: string | undefined | defaultState,
): connectionFailed => {
  return {
    type: 'CONNECTION_FAILED',
    payload: payload,
  };
};

const updateAccountRequest = (
  payload: string | undefined | defaultState,
): updateAccount => {
  return {
    type: 'UPDATE_ACCOUNT',
    payload: payload,
  };
};

export const updateAccounts = (account: string) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch(updateAccountRequest({ account: account }));
    dispatch(fetchData());
  };
};

export const bscMainnet = 97;

export const connect = () => {
  return async (
    dispatch: Dispatch<
      connectionDispatchTypes | connectionDataDispatchTypes | any
    >,
  ) => {
    dispatch(connectRequest());
    try {
      await window.ethereum.enable();
      const provider = new ethers.providers.Web3Provider(
        window.ethereum,
        'any',
      );
      if (window.ethereum) {
        try {
          const accounts = await provider.listAccounts();
          const networkId = await provider.getNetwork();
          const signer = provider.getSigner();
          if (networkId.chainId === bscMainnet) {
            const theContract = new ethers.Contract(
              '0x87f20296D21dEd8C7B8aAcFfA66DC709F236a594',
              HatSwapJson.abi,
              signer,
            );

            dispatch(
              connectSuccess({
                loading: false,
                smartContract: theContract,
                account: accounts[0],
                web3: provider,
              }),
            );

            window.ethereum.on('accountsChanged', (accounts) => {
              dispatch(updateAccounts(accounts[0]));
            });

            window.ethereum.on('chainChanged', () => {
              window.location.reload();
            });
          } else if (networkId.chainId !== bscMainnet) {
            dispatch(connectFailed('Change network to BSC MainNet'));
          }
        } catch (err: any) {
          dispatch(connectFailed(err.message));
        }
      } else if (window.ethereum === undefined) {
        dispatch(connectFailed('Please Install Metamask'));
      }
    } catch (err) {
      dispatch(connectFailed('Please Install Metamask'));
    }
  };
};
