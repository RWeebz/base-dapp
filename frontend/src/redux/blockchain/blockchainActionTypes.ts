import { ethers } from 'ethers';

export const CONNECTION_SUCCESS = 'CONNECTION_SUCCESS';
export const CONNECTION_FAILED = 'CONNECTION_FAILED';
export const CONNECTION_REQUEST = 'CONNECTION_REQUEST';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';

export interface connectionSuccess {
  type: typeof CONNECTION_SUCCESS;
  payload?: defaultState | any;
}

export interface connectionFailed {
  type: typeof CONNECTION_FAILED;
  payload?: defaultState | any;
}

export interface connectionRequest {
  type: typeof CONNECTION_REQUEST;
}

export interface updateAccount {
  type: typeof UPDATE_ACCOUNT;
  payload?: defaultState | any;
}

export type connectionDispatchTypes = connectionSuccess | connectionFailed | connectionRequest | updateAccount;

export type ExternalProvider = {
  isMetaMask?: boolean;
  isStatus?: boolean;
  host?: string;
  path?: string;
  sendAsync?: (request: { method: string; params?: Array<any> }, callback: (error: any, response: any) => void) => void;
  send?: (request: { method: string; params?: Array<any> }, callback: (error: any, response: any) => void) => void;
  request?: (request: { method: string; params?: Array<any> }) => Promise<any>;
};

export type defaultState = {
  loading?: boolean;
  account?: string | null | undefined;
  smartContract?: string | null | undefined | ethers.Contract;
  web3?: ethers.providers.Web3Provider | null | undefined;
  errorMsg?: string | null | undefined;
};

export interface errorMetamask {
  message: string;
  code: number;
}
