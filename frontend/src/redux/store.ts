import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import blockchainReducer from './blockchain/blockchainReducer';
import dataReducer from './data/dataReducer';

const middleware = [thunk];
const composeEnhancers = compose(applyMiddleware(...middleware));

const rootReducer = combineReducers({
  blockchain: blockchainReducer,
  data: dataReducer,
});

const configureStore = () => {
  return createStore(rootReducer, composeEnhancers);
};

const store = configureStore();

export type RootStore = ReturnType<typeof rootReducer>;

export type AppDispatch = typeof store.dispatch;

export default store;
