import { connect } from './blockchain/blockchainActions';
import {
  connectionDispatchTypes,
  connectionFailed,
  connectionRequest,
  connectionSuccess,
  updateAccount,
  defaultState,
} from './blockchain/blockchainActionTypes';
import {
  connectionDataDispatchTypes,
  checkDataFailed,
  checkDataRequest,
  checkDataSuccess,
  CHECK_DATA_FAILED,
  CHECK_DATA_REQUEST,
  CHECK_DATA_SUCCESS,
  dataDefaultState,
} from './data/dataActionTypes';
import { fetchData } from './data/dataActions';

export type {
  checkDataFailed,
  checkDataRequest,
  checkDataSuccess,
  dataDefaultState,
  connectionDataDispatchTypes,
  connectionDispatchTypes,
  connectionFailed,
  connectionRequest,
  connectionSuccess,
  updateAccount,
  defaultState,
};

export { connect, fetchData, CHECK_DATA_FAILED, CHECK_DATA_REQUEST, CHECK_DATA_SUCCESS };
