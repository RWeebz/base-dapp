export const CHECK_DATA_FAILED = 'CHECK_DATA_FAILED';
export const CHECK_DATA_SUCCESS = 'CHECK_DATA_SUCCESS';
export const CHECK_DATA_REQUEST = 'CHECK_DATA_REQUEST';

export type dataDefaultState = {
  loading?: boolean;
  name?: string;
  totalSupply?: string;
  error?: boolean;
  errorMsg?: string;
  allTokens?: Array<any>;
};

export interface checkDataRequest {
  type: typeof CHECK_DATA_REQUEST;
  payload?: dataDefaultState | any;
}
export interface checkDataFailed {
  type: typeof CHECK_DATA_FAILED;
  payload?: dataDefaultState | any;
}
export interface checkDataSuccess {
  type: typeof CHECK_DATA_SUCCESS;
  payload?: dataDefaultState | any;
}

export type connectionDataDispatchTypes =
  | checkDataFailed
  | checkDataRequest
  | checkDataSuccess;
