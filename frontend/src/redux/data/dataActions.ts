import { Dispatch } from 'redux';
import {
  checkDataFailed,
  checkDataRequest,
  checkDataSuccess,
  CHECK_DATA_FAILED,
  CHECK_DATA_REQUEST,
  CHECK_DATA_SUCCESS,
  connectionDataDispatchTypes,
  dataDefaultState,
} from '../index';
import store from '../store';

const fetchDataRequest = (): checkDataRequest => {
  return {
    type: CHECK_DATA_REQUEST,
  };
};

const fetchDataSuccess = (
  payload: string | undefined | dataDefaultState,
): checkDataSuccess => {
  return {
    type: CHECK_DATA_SUCCESS,
    payload: payload,
  };
};

const fetchDataFailed = (
  payload: string | undefined | dataDefaultState,
): checkDataFailed => {
  return {
    type: CHECK_DATA_FAILED,
    payload: payload,
  };
};

export const fetchData = () => {
  return async (dispatch: Dispatch<connectionDataDispatchTypes>) => {
    dispatch(fetchDataRequest());
    try {
      const name = await store.getState().blockchain.smartContract.name();
      const totalSupply = await store
        .getState()
        .blockchain.smartContract.totalSupply();
      dispatch(
        fetchDataSuccess({
          name: name,
          totalSupply: totalSupply,
        }),
      );
    } catch (err) {
      dispatch(fetchDataFailed('Could not load data from contract.'));
    }
  };
};
