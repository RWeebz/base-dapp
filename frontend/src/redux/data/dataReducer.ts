import { dataDefaultState, CHECK_DATA_FAILED, CHECK_DATA_REQUEST, CHECK_DATA_SUCCESS, connectionDataDispatchTypes } from '../index';

const initialState: dataDefaultState = {
  loading: false,
  name: '',
  allTokens: [],
  error: false,
  errorMsg: '',
};

const dataReducer = (state: dataDefaultState = initialState, action: connectionDataDispatchTypes) => {
  switch (action.type) {
    case CHECK_DATA_SUCCESS:
      return {
        ...initialState,
        loading: false,
        name: action.payload.name,
        allTokens: action.payload.allTokens,
      };
    case CHECK_DATA_FAILED:
      return {
        ...initialState,
        loading: false,
        error: true,
        errorMsg: action.payload,
      };
    case CHECK_DATA_REQUEST:
      return {
        ...initialState,
        loading: true,
      };
    default:
      return state;
  }
};

export default dataReducer;
