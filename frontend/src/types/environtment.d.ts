import { Ethereumish } from './EthereumTypes';

declare global {
  interface Window {
    ethereum: Ethereumish;
  }
}
