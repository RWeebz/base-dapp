import React, { useEffect, useState, useRef } from 'react';
import { ethers } from 'ethers';
import { useSelector, useDispatch } from 'react-redux';
import { connect, fetchData } from '../redux/index';
import { RootStore } from '../redux/store';
import { HatSwap } from '../typechain/types/HatSwap';

export default function Homepage() {
  const dispatch = useDispatch();
  const blockchain = useSelector((state: RootStore) => state.blockchain);
  const data = useSelector((state: RootStore) => state.data);
  const [accountBlockChain, setAccountBlockchain] = useState('');
  const contract: ethers.Contract | HatSwap = blockchain.smartContract;

  useEffect(() => {
    setAccountBlockchain(blockchain.account);
    if (blockchain.account !== '' && contract !== null) dispatch(fetchData());
  }, [contract, dispatch]);

  return (
    <div>
      {blockchain.account === '' || contract === null ? (
        <div>
          Connect to the BlockChain
          <button
            onClick={(e) => {
              e.preventDefault();
              dispatch(connect());
            }}
          >
            CONNECT
          </button>
          {blockchain.errorMsg !== '' ? <div>{blockchain.errorMsg}</div> : null}
        </div>
      ) : (
        <div>
          {blockchain.account}
          <div>
            <button
              onClick={async () => {
                // await contract.equipHat(1, {
                //   value: ethers.utils.parseEther('1'),
                // });
                await (contract as HatSwap).withdrawStuckBNB('');
              }}
            >
              Interact Contract
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
